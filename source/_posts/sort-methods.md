---
title: Sort In JavaScript
date: 2021-12-22 13:03:43
tags:
---

Sorting can be referred to as arranging files in some particular order. The arrangement performed can be based on the value of each file present. That particular order can be in either an ascending or descending fashion. 
Sorting algorithms are instructions given to a computer to arrange elements in a particular order.

## 1. How to sort strings ##
JavaScript arrays have a sort( ) method that sorts the array items into alphabetical order.

The sort( ) method accepts an optional argument which is a function that compares two elements of the array.

If the compare function is omitted, then the sort( ) method will sort the element based on the values of the elements.

Elements values rules:

1. If compare (a,b) is less than zero, the sort( ) method sorts a to a lower index than b. In other words, a will come first.
2. If compare (a,b) is greater than zero, the sort( ) method sort b to a lower index than a, i.e., b will come first.
3. If compare (a,b) returns zero, the sort ( ) method considers a equals b and leaves their positions unchanged.

Let's practice with an array of string named animals, as follows:

```
let animals = [
    'cat', 'dog', 'elephant', 'bee', 'ant'
];

```
To sort the elements of the animal's array in ascending order alphabetically, we need to use the sort( ) method without passing the compare function as in the example:

```
animals.sort();
console.log(animals);
// ["ant", "bee", "cat", "dog", "elephant"]

```

## 2. How to sort numbers ##

To sort an array of numbers in JavaScript, you can use the sort() method on the array object.

### 1. Sort the array in ascending order ###
To sort the numbers array in ascending order, you have to pass a function to the sort() method to customize its behaviour, and the function will be passed 2 elements a and b while iterating.

You have to subtract a-b and return the result.
```
const numArr = [3, 14, 1, 7, 4, 5, 6, 9];

numArr.sort((a, b) => {
  return a - b;
});

console.log(numArr); 
//Output:- [1, 3, 4, 5, 6, 7, 9, 14]
```
### 2. Sort the array in descending order ###
To sort the numbers array in ascending order, you have to pass a function to the sort() method to customize its behaviour, and the function will be passed 2 elements a and b while iterating.

You have to subtract b-a and return the result.

```
const numArr = [3, 14, 1, 7, 4, 5, 6, 9];

numArr.sort((a, b) => {
  return b - a;
});

console.log(numArr); 
//Output:- [14, 9, 7, 6, 5, 4, 3, 1]

```

## 3. How to sort dates ##

This is a simple solution that uses the sort() method to sort the array while using the new Date() to parse the string. This is not the most performant solution, but it is the simplest. For a performant solution, see below.

```
const arrayOfDates = [
  '2021-01-01',
  '2021-01-09',
  '2021-01-04',
  '2021-01-12'
]

arrayOfDates.sort((a, b) => {
  return new Date(a) - new Date(b)
})

```

This will sort the dates into ascending order. To sort in descending order, replace return new Date(a) - new Date(b) with return new Date(b) - new Date(a).

Sometimes, however, you may want to keep the original array. Using sort() on an array will sort the original array, so to retain the original array and sort the data in a new array, use array. slice().sort():
```

const arrayOfDates = [
  '2021-01-01',
  '2021-01-09',
  '2021-01-04',
  '2021-01-12'
]

const sortedArray = arrayOfDates.slice().sort((a, b) => {
  return new Date(a) - new Date(b)
})

```



