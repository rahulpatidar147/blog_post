---
title: How higher-order function chaining works
date: 2021-12-22 14:12:12
tags:
---


Higher-order functions are functions that operate on other functions, either by taking them as arguments or by returning them. In simple words, A Higher-Order function is a function that receives a function as an argument or returns the function as output.

For example, Array. prototype. map, Array.prototype.filter and Array.prototype.reduce are some of the Higher-Order functions built into the language.

## Filter ##
By using a filter() method, the method will return a new array with elements that pass the filtering rule (provide as a function).

Example:
```
const numbers = [3,6,1,9,4,5,10];

const newNumbers = numbers.filter(x=>x>3);

console.log(newNumbers);

```
Output
```
[ 6, 9, 5, 10 ]
```

## Map ##
We will map all the elements within an array by using a new function. For example, I want to have a new array whose contents are the result of every number multiplied by two.

Example:
```
const numbers = [1,2,3,4,5,6];

const newNumbers = numbers.map(n => n * 2);

console.log(newNumbers);

```
Output 
```
[2,4,6,8,10,12]
```
## Reduce ##
The last of the higher-order function example is the reduce() function. That used to do something to the elements contained in the array.

In this case, I want to sum the elements contained in the numbers array.

Example:
```

const numbers = [2,5,3,4,7,6];

const newNum = numbers.reduce((acc, curr) => acc + curr);

console.log(newNum);

```
Output
```
27
```


## Chaining Method ##
With chaining, we can combine all of the Higher-Order Functions in one execution.
```
const numbers = [2,5,3,4,7,6];

const total = numbers.filter(n => n > 3)

.map(n => n * 2) 
                
.reduce((accum, current) => accum + current); 

console.log(total);

```
Output:-
```
44
```
